// Given a set of strings, find the longest common prefix
// let input = [ 'on', 'off', 'one']

// - Creat React High Order component

// - React make api call [https://jsonplaceholder.typicode.com/albums]

// - Create a custom component (table with configurable paging) to show these records and the next page and previous page options

// React Directory Explorer with collapse expand on folders with interface and classes
// Add file/directory
// Rename
// Remove

// ===============

// Given a set of strings, find the longest common prefix
// let input = [ 'on', 'off', 'one']

const getCommonLongestPrefix = (inputArray) => {
	if (!inputArray?.length) {
		return "Kindly provide data in this format[ 'on', 'off', 'one'] ";
	}
	let prefix = inputArray[0];
	for (let i = 1; i < inputArray.length; i++) {
		while (inputArray[i].indexOf(prefix) !== 0) {
			// console.log(inputArray[i],prefix,inputArray[i].indexOf(prefix),inputArray[i].indexOf(prefix) !== 0)
			prefix = prefix.substring(0, prefix.length - 1);
			if (prefix === "") return "";
		}
	}
	return prefix;
};
let input = ["flower", "flow", "flight"];
let data = getCommonLongestPrefix(input);
console.log("Logest prefix : ", data || "Not exist");
