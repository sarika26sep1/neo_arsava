//===========Interface inheritance=========
interface ICollege {
	college_id: number;
	college_name: string;
}
interface IStudent extends ICollege {
	student_id: number;
	student_name: string;
}

let studentData: IStudent = {
	student_id: 1,
	student_name: "Omkar",
	college_id: 1,
	college_name: "ICOR",
};

console.log("studentData: ", studentData);



// =========Class inheritance=========
class College {
	collegeData: ICollege;

	constructor(data: ICollege) {
		this.collegeData = data;
	}
	getCollege() {
		return this.collegeData;
	}
}

class Student extends College {
	studentData: IStudent;
	constructor(data: IStudent) {
		super({ college_id: data.college_id, college_name: data.college_name });
		this.studentData = data;
	}
	getStudentData():IStudent {
		let collegeData = this.getCollege() || {};
        return  { ...this.studentData,...collegeData }
	}
}

let studentInstace = new Student({
	student_id: 1,
	student_name: "omkar",
	college_id: 1,
	college_name: "ICOER",
});

studentInstace.getStudentData();
