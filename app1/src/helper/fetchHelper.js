const API_ENDPOINT = process.env.REACT_APP_API_URL || "http://127.0.0.1:9000"; // to do: read it from the ENV
console.log("====", API_ENDPOINT);

const fetchApi = async (path, params = {}) => {
  try {
    let url = `${API_ENDPOINT}${path}`;

    const queryParams = { ...params };
    const queryString = new URLSearchParams(queryParams).toString();
    let newUrl = queryString ? `${url}?${queryString}` : `${url}`;

    const response = await fetch(newUrl);

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
  }
};

export const fetchApiPost = async (url, bodyData = {}) => {
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: bodyData ? JSON.stringify(bodyData) : null,
    });
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
  }
};

export const fetchApiDelete = async (path, bodyData = {}, params = {}) => {
  try {
    let url = `${API_ENDPOINT}${path}`;

    const queryParams = { ...params };
    const queryString = new URLSearchParams(queryParams).toString();
    let newUrl = queryString ? `${url}?${queryString}` : `${url}`;

    const response = await fetch(newUrl, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: bodyData ? JSON.stringify(bodyData) : null,
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
  }
};

export const fetchApiPut = async (path, bodyData = {}) => {
  try {
    let url = `${API_ENDPOINT}${path}`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: bodyData ? JSON.stringify(bodyData) : null,
    });
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
  }
};
export default fetchApi;
