import React, { useEffect, useState } from "react";
import fetchApi, {
  fetchApiDelete,
  fetchApiPut,
} from "../../helper/fetchHelper";
import { ListGroup, Button, Row, Col, Container } from "react-bootstrap";
import Modal from "../common/modal";
import ConfirmModal from "../common/confirmModal";
import "./style.css";
import { RiDeleteBin6Fill } from "react-icons/ri";
import { GoFileDirectory } from "react-icons/go";
import { FaFileAlt } from "react-icons/fa";
import { FaRegEdit } from "react-icons/fa";
import RenameModal from "../common/RenameModal";
function DirectoryList() {
  const [directory, setDirectory] = useState("./public_data");
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState([]);
  const [createType, setCreateType] = useState();
  const [temp, setTemp] = useState("");
  const [confirmModal, setConfirmModal] = useState(false);
  const [renameModal, setRenameModal] = useState(false);

  const getData = () => {
    let response = fetchApi("/directory", {
      url: directory,
    });
    response
      .then((data) => {
        setData(data?.data || []);
      })
      .catch((err) => {
        setData([]);
        console.log(err);
      });
  };

  const deleteDirectory = () => {
    let response = fetchApiDelete("/directory", {
      url: temp,
    });
    response
      .then((data) => {
        console.log(data);
        getData();
        setTemp("");
        setConfirmModal(false);
      })
      .catch((err) => {
        setData([]);
        console.log(err);
      });
  };
  const renameDirectory = (oldFileName, newFileName) => {
    let response = fetchApiPut("/directory", {
      oldFileName: `${directory}/${oldFileName}`,
      newFileName: `${directory}/${newFileName}`,
    });
    response
      .then((data) => {
        console.log(data);
        getData();
        setTemp("");
        setRenameModal(false);
      })
      .catch((err) => {
        setData([]);
        console.log(err);
      });
  };

  const deleteConfirm = (e, dir) => {
    e.stopPropagation();
    setConfirmModal(true);
    setTemp(dir);
  };
  const renameOperation = (e, dir) => {
    e.stopPropagation();
    setRenameModal(true);
    setTemp(dir);
  };

  useEffect(() => {
    getData();
  }, [directory]);

  return (
    <div className="directory-main-div">
      <Container>
        <Row>
          <Col md="6">{directory}</Col>{" "}
          <Col md="6">
            <Button
              variant="primary"
              onClick={() => {
                setShowModal(!showModal);
                setCreateType("directory");
              }}
            >
              Add Folder
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                setShowModal(!showModal);
                setCreateType("file");
              }}
            >
              Add File
            </Button>
            <Button
              onClick={() => {
                const lastIndex = directory.lastIndexOf("/");
                const slicedUrl = directory.substring(0, lastIndex);
                const occurrences = slicedUrl.match(/\//g);
                occurrences?.length > 1
                  ? setDirectory(slicedUrl)
                  : setDirectory("./public_data");
                // setDirectory(slicedUrl)
              }}
            >
              Back
            </Button>
          </Col>
        </Row>

        <ListGroup as="ul">
          {data && data?.length ? (
            data.map((files, index) => (
              <ListGroup.Item
                variant="info"
                as="li"
                onClick={() => {
                  files.isDirectory &&
                    setDirectory(`${directory}/${files.name}`);
                }}
                className="directory-list-item"
              >
                {files.isDirectory ? (
                  <div>
                    <GoFileDirectory />
                    {files.name}
                  </div>
                ) : (
                  <div>
                    <FaFileAlt />
                    {files.name}
                  </div>
                )}
                <div className="d-flex justify-content-between">
                  <div className="directory-list-item-icon" title="Rename">
                    <FaRegEdit
                      onClick={(e) => renameOperation(e, files.name)}
                    />
                  </div>
                  <div className="directory-list-item-icon" title="Remove">
                    <RiDeleteBin6Fill
                      onClick={(e) =>
                        deleteConfirm(e, `${directory}/${files.name}`)
                      }
                    />
                  </div>
                </div>
              </ListGroup.Item>
            ))
          ) : (
            <ListGroup as="ul">
              <ListGroup.Item variant="info" as="li">
                No files or directories
              </ListGroup.Item>
            </ListGroup>
          )}
        </ListGroup>
        <Modal
          showModal={showModal}
          setShowModal={setShowModal}
          createType={createType}
          directory={directory}
          getData={getData}
        />
        <ConfirmModal
          showModal={confirmModal}
          setShowModal={setConfirmModal}
          message={"Are you sure you want to delete this directory/file?"}
          handleSubmit={deleteDirectory}
        />
        <RenameModal
          showModal={renameModal}
          setShowModal={setRenameModal}
          directory={temp}
          handleSubmit={renameDirectory}
        />
      </Container>
    </div>
  );
}

export default DirectoryList;
