import React, { useEffect, useState } from "react";

import TableComponent from "../common/tableComponent";
import PaginationComponent from "../common/paginationComponent";
import {Container} from 'react-bootstrap'

function UserList() {
  const [users, setUsers] = useState([null]);
  const [tableData, setTableData] = useState([]);

  const headers = ["Id", "User ID", "Title"];
  const keys = ["id", "userId", "title"];

  const getApiData = async (url) => {
    await fetch("https://jsonplaceholder.typicode.com/todos/").then(
      async (response) => {
        let data = await response.json();
        setUsers(data || []);
        setTableData(data.slice(0, 10));
      }
    );
  };

  const getTableData = (tableData) => {
    setTableData(tableData);
  };

  useEffect(() => {
    getApiData();
  }, []);

  return (
    // <section className="list">
      <Container>
      <TableComponent headers={headers} data={tableData} keys={keys} />
      {users.length > 10 && (
        <PaginationComponent
          total={users}
          tableData={getTableData}
          isLoading={Boolean(tableData)}
        />
      )}
      </Container>
    // </section>
  );
}

export default UserList;
