import React, { useState, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import fetchApi from "../../helper/fetchHelper";

function AddModal(props) {
	const [show, setShow] = useState(false);
	const [type, setType] = useState(false);
	const { createType, showModal, setShowModal, directory } = props;
	const [textInput, setTextInput] = useState("");

	useEffect(() => {
		setShow(props?.showModal);
	}, [showModal]);

	useEffect(() => {
		setType(props?.createType);
	}, [createType]);

	const handleClose = () => {
		setShowModal(false);
		setShow(false);
	};
	//   const handleShow = () => setShow(true);

	const handleSubmit = () => {
		// Handle form submission here
		console.log("Text input:", textInput);
		if (textInput) {
			let response = fetchApi("/createdirectory", {
				url: type == "directory" ? `${directory}/${textInput}` : `${directory}`,
				fileName: type == "directory" ? "" : `/${textInput}`,
			});

			response.then((data) => {
				setTextInput("");
				props?.getData();
			});
		}

		handleClose();
	};

	return (
		<>
			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Add {type}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="formTextInput">
						{/* <Form.Label>Enter name {type}</Form.Label> */}
						<Form.Control
							type="text"
							placeholder={`Enter name of ${type}`}
							value={textInput}
							onChange={(e) => setTextInput(e.target.value)}
						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>
						Close
					</Button>
					<Button variant="primary" onClick={handleSubmit}>
						Submit
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
}

export default AddModal;
