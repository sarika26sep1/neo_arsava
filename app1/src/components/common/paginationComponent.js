import { Button, Spinner } from "react-bootstrap";
import React, { useRef, useState } from "react";

function PaginationComponent({ isLoading, total, tableData }) {
  const [currentPage, setCurrentPage] = useState(1);
  const perPage = 10;

  const pageRef = useRef();

  const fnGo = () => {
    let currPage = Number(pageRef.current.value);
    if (isNaN(currPage) || !currPage) {
      return;
    }
    fnPrepareTableData(currPage);
  };

  const fnPrepareTableData = (currPage) => {
    let end = currPage * perPage;
    let start = end - perPage;
    let data = total.slice(start, end);
    setCurrentPage(currPage);
    tableData(data);
  };

  const fnNext = () => {
    let currPage = currentPage + 1;
    fnPrepareTableData(currPage);
  };

  const fnPre = () => {
    let currPage = currentPage - 1;
    fnPrepareTableData(currPage);
  };

  return (
    <div className="pagination-component">
      {isLoading ? (
        <>
         <div>Total Pages : {total.length / perPage}</div>
          <div className="pagi-btn">
            <Button variant="primary" onClick={fnPre} disabled={currentPage === 1}>
              {"Prev"}
            </Button>
            <span className="pagi-span">{currentPage}</span>
            <Button
              variant="primary"
              onClick={fnNext}
              disabled={currentPage === total.length / perPage}
            >
              {"Next"}
            </Button>
          </div>
          {/* <div>
            <input
              className="page-input"
              ref={pageRef}
              placeholder="Page No."
            />
            <Button variant="primary" onClick={fnGo}>
              Go{" "}
            </Button>
          </div> */}
        </>
      ) : (
        <>
          <Button variant="danger" disabled>
            <Spinner
              as="span"
              animation="grow"
              size="sm"
              role="status"
              aria-hidden="true"
            />
            Loading...
          </Button>
        </>
      )}
    </div>
  );
}
export default PaginationComponent;
