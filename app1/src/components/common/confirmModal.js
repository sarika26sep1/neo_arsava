import React, { useState, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import fetchApi from "../../helper/fetchHelper";

function ConfirmModal(props) {
  const [show, setShow] = useState(false);
  const { showModal, setShowModal, directory, handleSubmit, message } = props;

  useEffect(() => {
    setShow(props?.showModal);
  }, [showModal]);

  const handleClose = () => {
    setShowModal(false);
    setShow(false);
  };
  //   const handleShow = () => setShow(true);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{message}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleSubmit}>
            Confirm
          </Button>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ConfirmModal;
