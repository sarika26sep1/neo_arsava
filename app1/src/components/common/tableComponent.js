import React from "react";
import { Table, Button, Spinner } from "react-bootstrap";

function TableComponent({ data, headers, keys }) {
  return (
    <div className="table-component">
      {data === undefined ? (
        <>
          <Button variant="danger" disabled>
            <Spinner
              as="span"
              animation="grow"
              size="sm"
              role="status"
              aria-hidden="true"
            />
            Loading...
          </Button>
        </>
      ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              {headers.length > 0 &&
                headers.map((v, i) => {
                  return <th>{v}</th>;
                })}
            </tr>
          </thead>
          <tbody>
            {data.length > 0 &&
              data.map((obj) => {
                return (
                  <tr>
                    {keys.length > 0 &&
                      keys.map((v) => {
                        return <td>{obj[v]}</td>;
                      })}
                  </tr>
                );
              })}
          </tbody>
        </Table>
      )}
    </div>
  );
}
export default TableComponent;
