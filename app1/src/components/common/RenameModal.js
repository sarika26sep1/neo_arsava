import React, { useState, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import fetchApi from "../../helper/fetchHelper";

function RenameModal(props) {
  const [show, setShow] = useState(false);
  const { showModal, setShowModal, directory, handleSubmit } = props;
  const [textInput, setTextInput] = useState(directory);

  useEffect(() => {
    setShow(props?.showModal);
  }, [showModal]);

  useEffect(() => {
    setTextInput(directory);
  }, [directory]);

  const handleClose = () => {
    setShowModal(false);
    setShow(false);
  };
  //   const handleShow = () => setShow(true);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Rename</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Label>Enter a new name</Form.Label>
          <Form.Control
            type="text"
            placeholder={`Enter new name`}
            value={textInput}
            onChange={(e) => setTextInput(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={() => handleSubmit(directory, textInput)}
          >
            Submit
          </Button>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default RenameModal;
