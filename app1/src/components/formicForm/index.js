import React from "react";
import "./style.css";
import { useFormik, Formik, Form, Field ,ErrorMessage} from "formik";
import * as Yup from "yup";

const FormmicForm = () => {
	const initialValues = {
		email: "",
		pasword: "",
		address:{
			city:"",
			state:""
		}
	};
	const validationSchema = Yup.object({
		email: Yup.string().email('Enter correct Eemail format').required("Email Required"),
		password: Yup.string().required("Password Required"),
		address:Yup.object({
			city:Yup.string().required("Required"),
			state:Yup.string().required("Required"),
		})
	});

	const onSubmit = (values) => {
		console.log("onSubmit", values);
	};

	// const formik = useFormik({
	// 	initialValues,
	// 	onSubmit,
	// 	validationSchema,
	// });

	// console.log("errors===", formik.errors);
	// console.log("toucehd===", formik.touched);
	return (
		<Formik
			initialValues={initialValues}
			onSubmit={onSubmit}
			validationSchema={validationSchema}
		>
			<Form>
				<label htmlFor="email">Email Address</label>
				<Field
					id="email"
					name="email"
					type="email"
					// onChange={formik.handleChange}
					// value={formik.values.email}
					// onBlur={formik.handleBlur}
				/>
				<br/>
				<ErrorMessage name="email"/>
				<br/>
				<label htmlFor="password">Password</label>
				<Field
					id="password"
					name="password"
					type="password"
				/>
				<br/>
				<ErrorMessage name="password"/>
				<br/>
				<button type="submit">Submit</button>

				<div>
					
				</div>
			</Form>

		</Formik>
	);
};

export default FormmicForm;
