import "./App.css";
import { useState } from "react";
import { Tabs, Tab, Card } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Directory from "./components/directory";
import UserList from "./components/user/userList";
import FormikForm from "./components/formicForm/index";

function App() {
	const [key, setKey] = useState("directory");
	return (
		<Card className="main-content">
			<Tabs
				className="mb-3"
				id="controlled-tab-example"
				activeKey={key}
				onSelect={(k) => setKey(k)}
			>	
				<Tab eventKey="directory" title="Directory">
					<Directory />
				</Tab>
				<Tab eventKey="userList" title="User List">
					<UserList />
				</Tab>
				<Tab eventKey="FormikForm" title="Formik Form">
					<FormikForm />
				</Tab>
			</Tabs>
		</Card>
	);
}

export default App;
