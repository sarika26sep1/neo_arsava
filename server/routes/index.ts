import { Router } from "express";
import Directory from "../service/directoryHelper";
const directoryHelper = new Directory();

const router = Router();
const public_data_url = "./public_data";

router.get("/directory", async (req, res) => {
  try {
    const { url } = req.query;
    const urlParam: string | undefined =
      typeof req.query.url === "string" ? req.query.url : undefined;
    console.log("Query Data", urlParam);

    let response = await directoryHelper.getDirectory(
      urlParam || public_data_url
    );
    res.status(200).send({ msg: "Success", data: response || [] });
  } catch (err) {
    res.status(500).send({ msg: "Error", data: [], error: err });
  }
});
router.get("/createdirectory", async (req, res) => {
  try {
    const { url, fileName } = req.query;
    const urlParam: string | undefined =
      typeof req.query.url === "string" ? req.query.url : undefined;
    const fileNameParam: string | undefined =
      typeof req.query.fileName === "string" ? req.query.fileName : undefined;
    console.log("Query Data", urlParam, fileNameParam);
    if (!urlParam) res.status(404).send({ msg: "Wrong data" });

    await directoryHelper.createDirectory(urlParam, fileNameParam);
    res.status(200).send({ msg: "Success" });
  } catch (err) {
    res.status(500).send({ msg: "Error", data: [], error: err });
  }
});

router.post("/directory", async (req, res) => {
  try {
    const { url, fileName } = req.body;
    const urlParam: string | undefined =
      typeof req.query.url === "string" ? req.query.url : undefined;
    const fileNameParam: string | undefined =
      typeof req.query.fileName === "string" ? req.query.fileName : undefined;
    console.log("Query Data", urlParam, fileNameParam);
    if (!urlParam) res.status(404).send({ msg: "Wrong data" });

    await directoryHelper.createDirectory(urlParam, fileNameParam);
    res.status(200).send({ msg: "Success" });
  } catch (err) {
    res.status(500).send({ msg: "Error", data: [], error: err });
  }
});
router.delete("/directory", async (req, res) => {
  try {
    let { url } = req.body;
    await directoryHelper.delete(url || "");
    res.status(200).send({ msg: "Success" });
  } catch (err) {
    res.status(500).send({ msg: "Error", data: [], error: err });
  }
});

router.put("/directory", async (req, res) => {
  try {
    let { oldFileName, newFileName } = req.body;
    await directoryHelper.renameFile(oldFileName, newFileName);
    res.status(200).send({ msg: "Success" });
  } catch (err) {
    res.status(500).send({ msg: "Error", data: [], error: err });
  }
});

export default router;
