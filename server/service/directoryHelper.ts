import fs from "fs/promises";
interface IDirectory {
  url: string;
  getDirectory: Function;
  createDirectory: Function;
  readFileContents: Function;
  delete: Function;
  renameFile: Function;
}
const public_data_url = "./public_data";
const getConcatUrl = (url: string | undefined | null): string => {
  return url ? `${public_data_url}/${url}` : public_data_url;
};
class Directory implements IDirectory {
  url: string;
  constructor() {
    this.url = "./";
  }

  async getDirectory(url?: string) {
    try {
      const files = await fs.readdir(url || public_data_url);
      const data = await Promise.all(
        files.map(async (file) => {
          const stats = await fs.stat(`${url}/${file}`);
          return {
            name: file,
            isDirectory: stats.isDirectory(),
          };
        })
      );
      return data;
    } catch (error) {
      console.error("Error reading directory:", error);
      throw [];
    }
  }
  async createDirectory(directoryPath: string | undefined, fileName?: string) {
    try {
      if (directoryPath) {
        // Create directory
        await fs.mkdir(directoryPath, { recursive: true });

        // Write file
        if (fileName) {
          const filePath = `${directoryPath}/${fileName}`;
          await fs.writeFile(filePath, "", "utf8");
          console.log(
            `File created successfully: ${directoryPath}/${fileName}`
          );
        }
        console.log(`Directory successfully: ${directoryPath}`);
      }
    } catch (error) {
      console.error("Error creating directory and file:", error);
      throw error;
    }
  }

  readFileContents(url: string) {
    return url;
  }

  async delete(url?: string) {
    try {
      if (url?.startsWith("./public_data")) {
        console.log("Path to delete ", url);
        await fs.rm(url, { recursive: true, force: true });
      }
    } catch (error) {
      console.error("Error deleting directory and file:", error);
      throw error;
    }
  }

  async renameFile(oldFileName: string, newFileName: string) {
    try {
      await fs.rename(oldFileName, newFileName);
      console.log("File renamed successfully.");
    } catch (err) {
      console.error("Error renaming file:", err);
    }
  }
}

export default Directory;
