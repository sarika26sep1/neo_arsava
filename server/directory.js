const fs = require('fs');

// Function to list directories, folders, and files in a given directory
function listDirectoryContents(directoryPath) {
    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            console.error('Error reading directory:', err);
            return;
        }

        // Log all files and directories in the directory
        files.forEach(file => {
            console.log(file);
        });
    });
}

// Example usage: list contents of the current directory
listDirectoryContents('./');